package com.sd.grpc.server.Model;

public class Medication_Related {
    private int mrID;
    private int mpID;
    private int mID;

    public int getMrID() {
        return mrID;
    }

    public void setMrID(int mrID) {
        this.mrID = mrID;
    }

    public int getMpID() {
        return mpID;
    }

    public void setMpID(int mpID) {
        this.mpID = mpID;
    }

    public int getmID() {
        return mID;
    }

    public void setmID(int mID) {
        this.mID = mID;
    }
}

package com.sd.grpc.server.Repos;

import com.sd.grpc.server.Database.DatabaseConnection;
import com.sd.grpc.server.Model.MedicationDb;
import com.sd.grpc.server.Model.Medication_Plan;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MedPlanRepository {

    public List<Medication_Plan> getAllPlans(int patientID) {
        List<Medication_Plan> list = new ArrayList<>();
        String query = "Select * from medication_plan where pID = ?";

        try (Connection connection = DatabaseConnection.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, patientID);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int mpID = resultSet.getInt("mpID");
                int pID = resultSet.getInt("pID");
                Date start_date = resultSet.getDate("start_date");
                Date end_date = resultSet.getDate("end_date");
                String intake = resultSet.getString("intake");
                Medication_Plan mp = new Medication_Plan();
                mp.setMpID(mpID);
                mp.setPatientID(pID);
                mp.setStart_date(start_date);
                mp.setEnd_date(end_date);
                mp.setIntake(intake);
                list.add(mp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public String getMedicationNameById(int medicationID) {
        String name = "";
        String query = "Select name from medication where medicationID = ?";
        try (Connection connection = DatabaseConnection.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, medicationID);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                name = resultSet.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return name;
    }

    public List<MedicationDb> getAllMedicationForPlan(int mpID) {
        List<MedicationDb> list = new ArrayList<>();
        String query = "Select * from medication_related where mpID = ?";

        try (Connection connection = DatabaseConnection.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, mpID);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int mID = resultSet.getInt("mID");
                MedicationDb medicationDb = new MedicationDb();
                medicationDb.setName(getMedicationNameById(mID));
                list.add(medicationDb);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}

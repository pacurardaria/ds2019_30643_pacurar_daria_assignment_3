package com.sd.grpc.server.Service;

import com.sd.grpc.*;
import com.sd.grpc.server.Managers.MedPlanManager;
import com.sd.grpc.server.Model.MedicationDb;
import com.sd.grpc.server.Model.Medication_Plan;

import java.util.List;

public class MedicationPlanService extends MessageServiceGrpc.MessageServiceImplBase {

    private static MedPlanManager medPlanManager= new MedPlanManager();

    @Override
    public void medicationTaken(MedicationStatus request, io.grpc.stub.StreamObserver<ServerResponse> responseObserver) {
        System.out.println("Medication taken");
        String status = request.getStatus();
        System.out.println("------ STATUS: " + status);

        ServerResponse.Builder builder = ServerResponse.newBuilder();
        builder.setServerResponse("ok");

        //send data back to client
        responseObserver.onNext(builder.build());
        responseObserver.onCompleted();
    }

    @Override
    public void medicationNotTaken(MedicationStatus request, io.grpc.stub.StreamObserver<ServerResponse> responseObserver) {
        System.out.println("Medication not taken");
        String status = request.getStatus();
        System.out.println("------ STATUS: " + status);

        ServerResponse.Builder builder = ServerResponse.newBuilder();
        builder.setServerResponse("not ok");

        //send data back to client
        responseObserver.onNext(builder.build());
        responseObserver.onCompleted();
    }

    @Override
    public void getAllMedicationPlans(MedicationPlanRequest request, io.grpc.stub.StreamObserver<MedicationPlanResponse> responseObserver) {
        String requestMsg = request.getMessage();
        MedicationPlanResponse.Builder builder = MedicationPlanResponse.newBuilder();

        System.out.println("------------------- Request message is: " + requestMsg);

        if (requestMsg.equals("get")) {

            List<Medication_Plan> list = medPlanManager.getAllPlans(6);

            for (Medication_Plan mp: list) {
                MedicationPlan.Builder medicationPlanBuilder = MedicationPlan.newBuilder();
                medicationPlanBuilder.setMedicationPlanId(mp.getMpID());
                //meds for each medication plan
                List<MedicationDb> medicationDbList = medPlanManager.getAllMedicationForPlan(mp.getMpID());

                for (MedicationDb m: medicationDbList) {
                    Medication.Builder medicationBuilder = Medication.newBuilder();
                    medicationBuilder.setName(m.getName());
                    System.out.println("---- Plan: " + mp.getMpID() + "---- Medication: " + m.getName());
                    medicationPlanBuilder.addMedication(medicationBuilder.build());
                }
                System.out.println("\n@@@@@@@@@@@\nMedication plan builder to string: " + medicationPlanBuilder.toString() + "\n@@@@@@@@@@@@\n");
                System.out.println("\n---- Plan: " + mp.getMpID() + "---- Start: " + mp.getStart_date() + "---- End: "+ mp.getEnd_date() + "---- Intake: "+ mp.getIntake());

                medicationPlanBuilder.setStartDate(mp.getStart_date().toString());
                medicationPlanBuilder.setEndDate(mp.getEnd_date().toString());
                medicationPlanBuilder.setIntake(mp.getIntake());
                builder.addMedicationPlanList(medicationPlanBuilder.build());
                System.out.println("\n@@@@@@@@@@@\nList builder to string: " + builder.toString() + "\n@@@@@@@@@@@@\n");
            }
        }
        responseObserver.onNext(builder.build());
        responseObserver.onCompleted();
    }
}

package com.sd.grpc.server.Managers;

import com.sd.grpc.server.Model.MedicationDb;
import com.sd.grpc.server.Repos.MedPlanRepository;
import com.sd.grpc.server.Model.Medication_Plan;

import java.util.List;

public class MedPlanManager {
    private MedPlanRepository medPlanRepository;

    public MedPlanManager() {
        this.medPlanRepository = new MedPlanRepository();
    }

    public List<Medication_Plan> getAllPlans(int patientID) {
        return medPlanRepository.getAllPlans(patientID);
    }

    public String getNameByID(int medicationID) {
        return medPlanRepository.getMedicationNameById(medicationID);
    }

    public List<MedicationDb> getAllMedicationForPlan(int mpID) {
        return medPlanRepository.getAllMedicationForPlan(mpID);
    }
}

package com.sd.grpc.server;

import com.sd.grpc.server.Service.MedicationPlanService;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;


public class MainServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        Server server = ServerBuilder.forPort(6565).addService(new MedicationPlanService()).build();
        server.start();

        System.out.println("Server started at: " + server.getPort());

        server.awaitTermination();
    }
}

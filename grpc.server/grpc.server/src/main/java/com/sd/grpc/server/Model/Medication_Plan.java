package com.sd.grpc.server.Model;

import java.sql.Date;

public class Medication_Plan {
    private int mpID;
    private int patientID;
    private Date start_date;
    private Date end_date;
    private String intake;

    public int getMpID() {
        return mpID;
    }

    public void setMpID(int mpID) {
        this.mpID = mpID;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public String getIntake() {
        return intake;
    }

    public void setIntake(String intake) {
        this.intake = intake;
    }

}

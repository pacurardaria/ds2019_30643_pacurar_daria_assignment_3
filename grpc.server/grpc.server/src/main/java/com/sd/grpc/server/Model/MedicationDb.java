package com.sd.grpc.server.Model;

public class MedicationDb {
    public int getMedicationID() {
        return medicationID;
    }

    private int medicationID;
    private String name;
    private int dosage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

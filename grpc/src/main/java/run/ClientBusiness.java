package run;

import com.sd.grpc.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class ClientBusiness {
    private MessageServiceGrpc.MessageServiceBlockingStub messageStub;

    public ClientBusiness(MessageServiceGrpc.MessageServiceBlockingStub messageStub) {
        this.messageStub = messageStub;
    }

    public void notifyMedicationNotTaken(Medication medication, MedicationPlan medicationPlan) {
        MedicationStatus medicationStatus = MedicationStatus.newBuilder()
                .setStatus(medicationPlan.getMedicationPlanId() + ", " + medication.getName() + ", " +medicationPlan.getIntake())
                .build();

        ServerResponse serverResponse = messageStub.medicationNotTaken(medicationStatus);
        System.out.println("\n  [Medication not taken -> Server response: " + serverResponse.getServerResponse());
    }

    public void notifyMedicationTaken(Medication medication, MedicationPlan medicationPlan) {
        MedicationStatus medicationStatus = MedicationStatus.newBuilder()
                .setStatus(medicationPlan.getMedicationPlanId() + ", " + medication.getName() + ", " +medicationPlan.getIntake())
                .build();

        ServerResponse serverResponse = messageStub.medicationTaken(medicationStatus);
        System.out.println("\n  [Medication taken -> Server response: " + serverResponse.getServerResponse());
    }

    public boolean checkIfCanTake(MedicationPlan medicationPlan) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

        String intake = medicationPlan.getIntake();
        String[] tokens = intake.split(" ");

        LocalTime start_date = LocalTime.parse(tokens[0],formatter);
        LocalTime end_date = LocalTime.parse(tokens[1],formatter);

        LocalTime current_date = LocalTime.now();

        if (current_date.isBefore(start_date)){
            return true;
        }

        if (current_date.isBefore(end_date) && current_date.isAfter(start_date)) {
            return true;
        } else {
            return false;
        }
    }
}

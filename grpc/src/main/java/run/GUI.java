package run;

import com.sd.grpc.*;
import io.grpc.ManagedChannel;

import javax.swing.*;
import java.awt.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static java.lang.Thread.sleep;

public class GUI {
    private JFrame frame = new JFrame("GRPC - CLIENT");
    private JLabel timeLabel;
    private JPanel childPanel;
    private JScrollPane scrollableArea;

    private ManagedChannel managedChannel;
    private MessageServiceGrpc.MessageServiceBlockingStub messageStub;

    private MedicationPlanResponse medicationPlanResponse;
    private MedicationPlanRequest medicationPlanRequest;
    private ClientBusiness clientBusiness;

    public GUI(ManagedChannel managedChannel, MessageServiceGrpc.MessageServiceBlockingStub messageStub, MedicationPlanResponse response, MedicationPlanRequest request) {
        this.medicationPlanResponse = response; // first time it gets the medication plans
        this.managedChannel = managedChannel;
        this.messageStub = messageStub;
        this.medicationPlanRequest = request;
        this.clientBusiness = new ClientBusiness(this.messageStub);

        frame.setLayout(null);
        frame.getContentPane().setLayout(new GridLayout(0,1));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);

        timeLabel = new JLabel("");
        timeLabel.setBounds(50, 50, 150, 50);
        timeLabel.setBackground(Color.yellow);

        childPanel = new JPanel();
        childPanel.setLayout(new GridLayout(0, 1));
        childPanel.add(timeLabel);

        showTime();
        runHourlyCheck();

        scrollableArea = new JScrollPane(childPanel);
        scrollableArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollableArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        frame.getContentPane().add(scrollableArea);
        frame.setVisible(true);
    }

    public void load(MedicationPlanResponse medicationPlanResponse) {
        for(MedicationPlan medicationPlan: medicationPlanResponse.getMedicationPlanListList()) {
            showMedicationPlan(medicationPlan);
        }
        this.medicationPlanResponse = medicationPlanResponse;
    }

    public void showTime() {
        Thread thread = new Thread(() -> {
            for(;;) {
                LocalTime time = LocalTime.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
                timeLabel.setText("Current time: " + time.format(formatter));
            }
        });
        thread.start();
    }

    public void runHourlyCheck() {
        Thread thread = new Thread(() -> {
            try {
                for(;;) {
                    checkHourlyForMedicationPlans();
                    sleep(10000); // 10 secunde
//                    SwingUtilities.updateComponentTreeUI(frame);
//                    frame.invalidate();
//                    frame.validate();
//                    frame.repaint();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }

    public void checkHourlyForMedicationPlans() {
        Thread thread = new Thread(() -> {
            MedicationPlanResponse medicationPlanResponse = messageStub.getAllMedicationPlans(medicationPlanRequest);
            for (MedicationPlan medicationPlan : medicationPlanResponse.getMedicationPlanListList()) {
                for (Medication medication : medicationPlan.getMedicationList()) {
                    if (!clientBusiness.checkIfCanTake(medicationPlan)) {
                        clientBusiness.notifyMedicationNotTaken(medication, medicationPlan);
                    }
                }
            }
        });
        thread.start();
    }

    public void showMedicationPlan(MedicationPlan medicationPlan) {
        JLabel medicationPlanID = new JLabel("> Medication Plan ID :" + medicationPlan.getMedicationPlanId());
        childPanel.add(medicationPlanID);
        for(Medication medication : medicationPlan.getMedicationList()) {
            showMedication(medication, medicationPlan);
        }
    }

    public void showMedication(Medication medication, MedicationPlan medicationPlan) {
        JPanel medicationPanel = new JPanel();
        medicationPanel.setLayout(new FlowLayout());

        JLabel medicationName = new JLabel(medication.getName());
        JLabel intakeInterval = new JLabel(medicationPlan.getIntake());
        JButton takeButton = new JButton("> Take med");

        if(clientBusiness.checkIfCanTake(medicationPlan)) {
            takeButton.addActionListener(e -> {
                intakeInterval.setText("> TAKEN <");
                intakeInterval.setBackground(Color.GREEN);
                takeButton.setVisible(false);
            /**
             * Notify the server that the medication has been taken.
             */
                clientBusiness.notifyMedicationTaken(medication, medicationPlan);
            });
        } else {
            intakeInterval.setText("> NOT TAKEN <");
            intakeInterval.setBackground(Color.RED);
            takeButton.setVisible(false);
            /**
             * Notify the server that the medication hasn't been taken.
             */
            clientBusiness.notifyMedicationNotTaken(medication, medicationPlan);
        }

        medicationPanel.add(medicationName);
        medicationPanel.add(intakeInterval);
        medicationPanel.add(takeButton);
        childPanel.add(medicationPanel);
    }
}

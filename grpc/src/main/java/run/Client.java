package run;

import com.sd.grpc.MedicationPlanRequest;
import com.sd.grpc.MedicationPlanResponse;
import com.sd.grpc.MessageServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class Client {
    public static void main(String[] args) {
        System.out.println("Started GRPC------------- CLIENT");
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",6565).usePlaintext().build();

        //client will wait for server response (blockingstub)
        MessageServiceGrpc.MessageServiceBlockingStub messageStub = MessageServiceGrpc.newBlockingStub(channel);

        //get medication from server
        MedicationPlanRequest request = MedicationPlanRequest.newBuilder()
                .setMessage("get")
                .build();

        MedicationPlanResponse response;
        response = messageStub.getAllMedicationPlans(request);
        //start UI
        GUI gui = new GUI(channel, messageStub, response, request);
        gui.load(response);
    }
}

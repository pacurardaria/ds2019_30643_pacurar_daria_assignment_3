package com.sd.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: grpc.proto")
public final class MessageServiceGrpc {

  private MessageServiceGrpc() {}

  public static final String SERVICE_NAME = "MessageService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.sd.grpc.MedicationStatus,
      com.sd.grpc.ServerResponse> getMedicationTakenMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "medicationTaken",
      requestType = com.sd.grpc.MedicationStatus.class,
      responseType = com.sd.grpc.ServerResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.sd.grpc.MedicationStatus,
      com.sd.grpc.ServerResponse> getMedicationTakenMethod() {
    io.grpc.MethodDescriptor<com.sd.grpc.MedicationStatus, com.sd.grpc.ServerResponse> getMedicationTakenMethod;
    if ((getMedicationTakenMethod = MessageServiceGrpc.getMedicationTakenMethod) == null) {
      synchronized (MessageServiceGrpc.class) {
        if ((getMedicationTakenMethod = MessageServiceGrpc.getMedicationTakenMethod) == null) {
          MessageServiceGrpc.getMedicationTakenMethod = getMedicationTakenMethod = 
              io.grpc.MethodDescriptor.<com.sd.grpc.MedicationStatus, com.sd.grpc.ServerResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MessageService", "medicationTaken"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.sd.grpc.MedicationStatus.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.sd.grpc.ServerResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new MessageServiceMethodDescriptorSupplier("medicationTaken"))
                  .build();
          }
        }
     }
     return getMedicationTakenMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.sd.grpc.MedicationStatus,
      com.sd.grpc.ServerResponse> getMedicationNotTakenMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "medicationNotTaken",
      requestType = com.sd.grpc.MedicationStatus.class,
      responseType = com.sd.grpc.ServerResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.sd.grpc.MedicationStatus,
      com.sd.grpc.ServerResponse> getMedicationNotTakenMethod() {
    io.grpc.MethodDescriptor<com.sd.grpc.MedicationStatus, com.sd.grpc.ServerResponse> getMedicationNotTakenMethod;
    if ((getMedicationNotTakenMethod = MessageServiceGrpc.getMedicationNotTakenMethod) == null) {
      synchronized (MessageServiceGrpc.class) {
        if ((getMedicationNotTakenMethod = MessageServiceGrpc.getMedicationNotTakenMethod) == null) {
          MessageServiceGrpc.getMedicationNotTakenMethod = getMedicationNotTakenMethod = 
              io.grpc.MethodDescriptor.<com.sd.grpc.MedicationStatus, com.sd.grpc.ServerResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MessageService", "medicationNotTaken"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.sd.grpc.MedicationStatus.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.sd.grpc.ServerResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new MessageServiceMethodDescriptorSupplier("medicationNotTaken"))
                  .build();
          }
        }
     }
     return getMedicationNotTakenMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.sd.grpc.MedicationPlanRequest,
      com.sd.grpc.MedicationPlanResponse> getGetAllMedicationPlansMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getAllMedicationPlans",
      requestType = com.sd.grpc.MedicationPlanRequest.class,
      responseType = com.sd.grpc.MedicationPlanResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.sd.grpc.MedicationPlanRequest,
      com.sd.grpc.MedicationPlanResponse> getGetAllMedicationPlansMethod() {
    io.grpc.MethodDescriptor<com.sd.grpc.MedicationPlanRequest, com.sd.grpc.MedicationPlanResponse> getGetAllMedicationPlansMethod;
    if ((getGetAllMedicationPlansMethod = MessageServiceGrpc.getGetAllMedicationPlansMethod) == null) {
      synchronized (MessageServiceGrpc.class) {
        if ((getGetAllMedicationPlansMethod = MessageServiceGrpc.getGetAllMedicationPlansMethod) == null) {
          MessageServiceGrpc.getGetAllMedicationPlansMethod = getGetAllMedicationPlansMethod = 
              io.grpc.MethodDescriptor.<com.sd.grpc.MedicationPlanRequest, com.sd.grpc.MedicationPlanResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MessageService", "getAllMedicationPlans"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.sd.grpc.MedicationPlanRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.sd.grpc.MedicationPlanResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new MessageServiceMethodDescriptorSupplier("getAllMedicationPlans"))
                  .build();
          }
        }
     }
     return getGetAllMedicationPlansMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static MessageServiceStub newStub(io.grpc.Channel channel) {
    return new MessageServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static MessageServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new MessageServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static MessageServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new MessageServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class MessageServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void medicationTaken(com.sd.grpc.MedicationStatus request,
        io.grpc.stub.StreamObserver<com.sd.grpc.ServerResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getMedicationTakenMethod(), responseObserver);
    }

    /**
     */
    public void medicationNotTaken(com.sd.grpc.MedicationStatus request,
        io.grpc.stub.StreamObserver<com.sd.grpc.ServerResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getMedicationNotTakenMethod(), responseObserver);
    }

    /**
     */
    public void getAllMedicationPlans(com.sd.grpc.MedicationPlanRequest request,
        io.grpc.stub.StreamObserver<com.sd.grpc.MedicationPlanResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetAllMedicationPlansMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getMedicationTakenMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.sd.grpc.MedicationStatus,
                com.sd.grpc.ServerResponse>(
                  this, METHODID_MEDICATION_TAKEN)))
          .addMethod(
            getMedicationNotTakenMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.sd.grpc.MedicationStatus,
                com.sd.grpc.ServerResponse>(
                  this, METHODID_MEDICATION_NOT_TAKEN)))
          .addMethod(
            getGetAllMedicationPlansMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.sd.grpc.MedicationPlanRequest,
                com.sd.grpc.MedicationPlanResponse>(
                  this, METHODID_GET_ALL_MEDICATION_PLANS)))
          .build();
    }
  }

  /**
   */
  public static final class MessageServiceStub extends io.grpc.stub.AbstractStub<MessageServiceStub> {
    private MessageServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MessageServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MessageServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MessageServiceStub(channel, callOptions);
    }

    /**
     */
    public void medicationTaken(com.sd.grpc.MedicationStatus request,
        io.grpc.stub.StreamObserver<com.sd.grpc.ServerResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getMedicationTakenMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void medicationNotTaken(com.sd.grpc.MedicationStatus request,
        io.grpc.stub.StreamObserver<com.sd.grpc.ServerResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getMedicationNotTakenMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getAllMedicationPlans(com.sd.grpc.MedicationPlanRequest request,
        io.grpc.stub.StreamObserver<com.sd.grpc.MedicationPlanResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetAllMedicationPlansMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class MessageServiceBlockingStub extends io.grpc.stub.AbstractStub<MessageServiceBlockingStub> {
    private MessageServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MessageServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MessageServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MessageServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.sd.grpc.ServerResponse medicationTaken(com.sd.grpc.MedicationStatus request) {
      return blockingUnaryCall(
          getChannel(), getMedicationTakenMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.sd.grpc.ServerResponse medicationNotTaken(com.sd.grpc.MedicationStatus request) {
      return blockingUnaryCall(
          getChannel(), getMedicationNotTakenMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.sd.grpc.MedicationPlanResponse getAllMedicationPlans(com.sd.grpc.MedicationPlanRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetAllMedicationPlansMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class MessageServiceFutureStub extends io.grpc.stub.AbstractStub<MessageServiceFutureStub> {
    private MessageServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MessageServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MessageServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MessageServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.sd.grpc.ServerResponse> medicationTaken(
        com.sd.grpc.MedicationStatus request) {
      return futureUnaryCall(
          getChannel().newCall(getMedicationTakenMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.sd.grpc.ServerResponse> medicationNotTaken(
        com.sd.grpc.MedicationStatus request) {
      return futureUnaryCall(
          getChannel().newCall(getMedicationNotTakenMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.sd.grpc.MedicationPlanResponse> getAllMedicationPlans(
        com.sd.grpc.MedicationPlanRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetAllMedicationPlansMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_MEDICATION_TAKEN = 0;
  private static final int METHODID_MEDICATION_NOT_TAKEN = 1;
  private static final int METHODID_GET_ALL_MEDICATION_PLANS = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final MessageServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(MessageServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_MEDICATION_TAKEN:
          serviceImpl.medicationTaken((com.sd.grpc.MedicationStatus) request,
              (io.grpc.stub.StreamObserver<com.sd.grpc.ServerResponse>) responseObserver);
          break;
        case METHODID_MEDICATION_NOT_TAKEN:
          serviceImpl.medicationNotTaken((com.sd.grpc.MedicationStatus) request,
              (io.grpc.stub.StreamObserver<com.sd.grpc.ServerResponse>) responseObserver);
          break;
        case METHODID_GET_ALL_MEDICATION_PLANS:
          serviceImpl.getAllMedicationPlans((com.sd.grpc.MedicationPlanRequest) request,
              (io.grpc.stub.StreamObserver<com.sd.grpc.MedicationPlanResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class MessageServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    MessageServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.sd.grpc.Grpc.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("MessageService");
    }
  }

  private static final class MessageServiceFileDescriptorSupplier
      extends MessageServiceBaseDescriptorSupplier {
    MessageServiceFileDescriptorSupplier() {}
  }

  private static final class MessageServiceMethodDescriptorSupplier
      extends MessageServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    MessageServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (MessageServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new MessageServiceFileDescriptorSupplier())
              .addMethod(getMedicationTakenMethod())
              .addMethod(getMedicationNotTakenMethod())
              .addMethod(getGetAllMedicationPlansMethod())
              .build();
        }
      }
    }
    return result;
  }
}

// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: grpc.proto

package com.sd.grpc;

/**
 * Protobuf type {@code MedicationPlanResponse}
 */
public  final class MedicationPlanResponse extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:MedicationPlanResponse)
    MedicationPlanResponseOrBuilder {
private static final long serialVersionUID = 0L;
  // Use MedicationPlanResponse.newBuilder() to construct.
  private MedicationPlanResponse(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private MedicationPlanResponse() {
    medicationPlanList_ = java.util.Collections.emptyList();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private MedicationPlanResponse(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 10: {
            if (!((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
              medicationPlanList_ = new java.util.ArrayList<com.sd.grpc.MedicationPlan>();
              mutable_bitField0_ |= 0x00000001;
            }
            medicationPlanList_.add(
                input.readMessage(com.sd.grpc.MedicationPlan.parser(), extensionRegistry));
            break;
          }
          default: {
            if (!parseUnknownFieldProto3(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      if (((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
        medicationPlanList_ = java.util.Collections.unmodifiableList(medicationPlanList_);
      }
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.sd.grpc.Grpc.internal_static_MedicationPlanResponse_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.sd.grpc.Grpc.internal_static_MedicationPlanResponse_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.sd.grpc.MedicationPlanResponse.class, com.sd.grpc.MedicationPlanResponse.Builder.class);
  }

  public static final int MEDICATIONPLANLIST_FIELD_NUMBER = 1;
  private java.util.List<com.sd.grpc.MedicationPlan> medicationPlanList_;
  /**
   * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
   */
  public java.util.List<com.sd.grpc.MedicationPlan> getMedicationPlanListList() {
    return medicationPlanList_;
  }
  /**
   * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
   */
  public java.util.List<? extends com.sd.grpc.MedicationPlanOrBuilder> 
      getMedicationPlanListOrBuilderList() {
    return medicationPlanList_;
  }
  /**
   * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
   */
  public int getMedicationPlanListCount() {
    return medicationPlanList_.size();
  }
  /**
   * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
   */
  public com.sd.grpc.MedicationPlan getMedicationPlanList(int index) {
    return medicationPlanList_.get(index);
  }
  /**
   * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
   */
  public com.sd.grpc.MedicationPlanOrBuilder getMedicationPlanListOrBuilder(
      int index) {
    return medicationPlanList_.get(index);
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    for (int i = 0; i < medicationPlanList_.size(); i++) {
      output.writeMessage(1, medicationPlanList_.get(i));
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    for (int i = 0; i < medicationPlanList_.size(); i++) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, medicationPlanList_.get(i));
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.sd.grpc.MedicationPlanResponse)) {
      return super.equals(obj);
    }
    com.sd.grpc.MedicationPlanResponse other = (com.sd.grpc.MedicationPlanResponse) obj;

    boolean result = true;
    result = result && getMedicationPlanListList()
        .equals(other.getMedicationPlanListList());
    result = result && unknownFields.equals(other.unknownFields);
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (getMedicationPlanListCount() > 0) {
      hash = (37 * hash) + MEDICATIONPLANLIST_FIELD_NUMBER;
      hash = (53 * hash) + getMedicationPlanListList().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.sd.grpc.MedicationPlanResponse parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.sd.grpc.MedicationPlanResponse parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.sd.grpc.MedicationPlanResponse parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.sd.grpc.MedicationPlanResponse parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.sd.grpc.MedicationPlanResponse parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.sd.grpc.MedicationPlanResponse parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.sd.grpc.MedicationPlanResponse parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.sd.grpc.MedicationPlanResponse parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.sd.grpc.MedicationPlanResponse parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.sd.grpc.MedicationPlanResponse parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.sd.grpc.MedicationPlanResponse parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.sd.grpc.MedicationPlanResponse parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.sd.grpc.MedicationPlanResponse prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code MedicationPlanResponse}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:MedicationPlanResponse)
      com.sd.grpc.MedicationPlanResponseOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.sd.grpc.Grpc.internal_static_MedicationPlanResponse_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.sd.grpc.Grpc.internal_static_MedicationPlanResponse_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.sd.grpc.MedicationPlanResponse.class, com.sd.grpc.MedicationPlanResponse.Builder.class);
    }

    // Construct using com.sd.grpc.MedicationPlanResponse.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
        getMedicationPlanListFieldBuilder();
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      if (medicationPlanListBuilder_ == null) {
        medicationPlanList_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
      } else {
        medicationPlanListBuilder_.clear();
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.sd.grpc.Grpc.internal_static_MedicationPlanResponse_descriptor;
    }

    @java.lang.Override
    public com.sd.grpc.MedicationPlanResponse getDefaultInstanceForType() {
      return com.sd.grpc.MedicationPlanResponse.getDefaultInstance();
    }

    @java.lang.Override
    public com.sd.grpc.MedicationPlanResponse build() {
      com.sd.grpc.MedicationPlanResponse result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public com.sd.grpc.MedicationPlanResponse buildPartial() {
      com.sd.grpc.MedicationPlanResponse result = new com.sd.grpc.MedicationPlanResponse(this);
      int from_bitField0_ = bitField0_;
      if (medicationPlanListBuilder_ == null) {
        if (((bitField0_ & 0x00000001) == 0x00000001)) {
          medicationPlanList_ = java.util.Collections.unmodifiableList(medicationPlanList_);
          bitField0_ = (bitField0_ & ~0x00000001);
        }
        result.medicationPlanList_ = medicationPlanList_;
      } else {
        result.medicationPlanList_ = medicationPlanListBuilder_.build();
      }
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return (Builder) super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.sd.grpc.MedicationPlanResponse) {
        return mergeFrom((com.sd.grpc.MedicationPlanResponse)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.sd.grpc.MedicationPlanResponse other) {
      if (other == com.sd.grpc.MedicationPlanResponse.getDefaultInstance()) return this;
      if (medicationPlanListBuilder_ == null) {
        if (!other.medicationPlanList_.isEmpty()) {
          if (medicationPlanList_.isEmpty()) {
            medicationPlanList_ = other.medicationPlanList_;
            bitField0_ = (bitField0_ & ~0x00000001);
          } else {
            ensureMedicationPlanListIsMutable();
            medicationPlanList_.addAll(other.medicationPlanList_);
          }
          onChanged();
        }
      } else {
        if (!other.medicationPlanList_.isEmpty()) {
          if (medicationPlanListBuilder_.isEmpty()) {
            medicationPlanListBuilder_.dispose();
            medicationPlanListBuilder_ = null;
            medicationPlanList_ = other.medicationPlanList_;
            bitField0_ = (bitField0_ & ~0x00000001);
            medicationPlanListBuilder_ = 
              com.google.protobuf.GeneratedMessageV3.alwaysUseFieldBuilders ?
                 getMedicationPlanListFieldBuilder() : null;
          } else {
            medicationPlanListBuilder_.addAllMessages(other.medicationPlanList_);
          }
        }
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.sd.grpc.MedicationPlanResponse parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.sd.grpc.MedicationPlanResponse) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private java.util.List<com.sd.grpc.MedicationPlan> medicationPlanList_ =
      java.util.Collections.emptyList();
    private void ensureMedicationPlanListIsMutable() {
      if (!((bitField0_ & 0x00000001) == 0x00000001)) {
        medicationPlanList_ = new java.util.ArrayList<com.sd.grpc.MedicationPlan>(medicationPlanList_);
        bitField0_ |= 0x00000001;
       }
    }

    private com.google.protobuf.RepeatedFieldBuilderV3<
        com.sd.grpc.MedicationPlan, com.sd.grpc.MedicationPlan.Builder, com.sd.grpc.MedicationPlanOrBuilder> medicationPlanListBuilder_;

    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public java.util.List<com.sd.grpc.MedicationPlan> getMedicationPlanListList() {
      if (medicationPlanListBuilder_ == null) {
        return java.util.Collections.unmodifiableList(medicationPlanList_);
      } else {
        return medicationPlanListBuilder_.getMessageList();
      }
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public int getMedicationPlanListCount() {
      if (medicationPlanListBuilder_ == null) {
        return medicationPlanList_.size();
      } else {
        return medicationPlanListBuilder_.getCount();
      }
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public com.sd.grpc.MedicationPlan getMedicationPlanList(int index) {
      if (medicationPlanListBuilder_ == null) {
        return medicationPlanList_.get(index);
      } else {
        return medicationPlanListBuilder_.getMessage(index);
      }
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public Builder setMedicationPlanList(
        int index, com.sd.grpc.MedicationPlan value) {
      if (medicationPlanListBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureMedicationPlanListIsMutable();
        medicationPlanList_.set(index, value);
        onChanged();
      } else {
        medicationPlanListBuilder_.setMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public Builder setMedicationPlanList(
        int index, com.sd.grpc.MedicationPlan.Builder builderForValue) {
      if (medicationPlanListBuilder_ == null) {
        ensureMedicationPlanListIsMutable();
        medicationPlanList_.set(index, builderForValue.build());
        onChanged();
      } else {
        medicationPlanListBuilder_.setMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public Builder addMedicationPlanList(com.sd.grpc.MedicationPlan value) {
      if (medicationPlanListBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureMedicationPlanListIsMutable();
        medicationPlanList_.add(value);
        onChanged();
      } else {
        medicationPlanListBuilder_.addMessage(value);
      }
      return this;
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public Builder addMedicationPlanList(
        int index, com.sd.grpc.MedicationPlan value) {
      if (medicationPlanListBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureMedicationPlanListIsMutable();
        medicationPlanList_.add(index, value);
        onChanged();
      } else {
        medicationPlanListBuilder_.addMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public Builder addMedicationPlanList(
        com.sd.grpc.MedicationPlan.Builder builderForValue) {
      if (medicationPlanListBuilder_ == null) {
        ensureMedicationPlanListIsMutable();
        medicationPlanList_.add(builderForValue.build());
        onChanged();
      } else {
        medicationPlanListBuilder_.addMessage(builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public Builder addMedicationPlanList(
        int index, com.sd.grpc.MedicationPlan.Builder builderForValue) {
      if (medicationPlanListBuilder_ == null) {
        ensureMedicationPlanListIsMutable();
        medicationPlanList_.add(index, builderForValue.build());
        onChanged();
      } else {
        medicationPlanListBuilder_.addMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public Builder addAllMedicationPlanList(
        java.lang.Iterable<? extends com.sd.grpc.MedicationPlan> values) {
      if (medicationPlanListBuilder_ == null) {
        ensureMedicationPlanListIsMutable();
        com.google.protobuf.AbstractMessageLite.Builder.addAll(
            values, medicationPlanList_);
        onChanged();
      } else {
        medicationPlanListBuilder_.addAllMessages(values);
      }
      return this;
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public Builder clearMedicationPlanList() {
      if (medicationPlanListBuilder_ == null) {
        medicationPlanList_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
        onChanged();
      } else {
        medicationPlanListBuilder_.clear();
      }
      return this;
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public Builder removeMedicationPlanList(int index) {
      if (medicationPlanListBuilder_ == null) {
        ensureMedicationPlanListIsMutable();
        medicationPlanList_.remove(index);
        onChanged();
      } else {
        medicationPlanListBuilder_.remove(index);
      }
      return this;
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public com.sd.grpc.MedicationPlan.Builder getMedicationPlanListBuilder(
        int index) {
      return getMedicationPlanListFieldBuilder().getBuilder(index);
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public com.sd.grpc.MedicationPlanOrBuilder getMedicationPlanListOrBuilder(
        int index) {
      if (medicationPlanListBuilder_ == null) {
        return medicationPlanList_.get(index);  } else {
        return medicationPlanListBuilder_.getMessageOrBuilder(index);
      }
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public java.util.List<? extends com.sd.grpc.MedicationPlanOrBuilder> 
         getMedicationPlanListOrBuilderList() {
      if (medicationPlanListBuilder_ != null) {
        return medicationPlanListBuilder_.getMessageOrBuilderList();
      } else {
        return java.util.Collections.unmodifiableList(medicationPlanList_);
      }
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public com.sd.grpc.MedicationPlan.Builder addMedicationPlanListBuilder() {
      return getMedicationPlanListFieldBuilder().addBuilder(
          com.sd.grpc.MedicationPlan.getDefaultInstance());
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public com.sd.grpc.MedicationPlan.Builder addMedicationPlanListBuilder(
        int index) {
      return getMedicationPlanListFieldBuilder().addBuilder(
          index, com.sd.grpc.MedicationPlan.getDefaultInstance());
    }
    /**
     * <code>repeated .MedicationPlan medicationPlanList = 1;</code>
     */
    public java.util.List<com.sd.grpc.MedicationPlan.Builder> 
         getMedicationPlanListBuilderList() {
      return getMedicationPlanListFieldBuilder().getBuilderList();
    }
    private com.google.protobuf.RepeatedFieldBuilderV3<
        com.sd.grpc.MedicationPlan, com.sd.grpc.MedicationPlan.Builder, com.sd.grpc.MedicationPlanOrBuilder> 
        getMedicationPlanListFieldBuilder() {
      if (medicationPlanListBuilder_ == null) {
        medicationPlanListBuilder_ = new com.google.protobuf.RepeatedFieldBuilderV3<
            com.sd.grpc.MedicationPlan, com.sd.grpc.MedicationPlan.Builder, com.sd.grpc.MedicationPlanOrBuilder>(
                medicationPlanList_,
                ((bitField0_ & 0x00000001) == 0x00000001),
                getParentForChildren(),
                isClean());
        medicationPlanList_ = null;
      }
      return medicationPlanListBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFieldsProto3(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:MedicationPlanResponse)
  }

  // @@protoc_insertion_point(class_scope:MedicationPlanResponse)
  private static final com.sd.grpc.MedicationPlanResponse DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.sd.grpc.MedicationPlanResponse();
  }

  public static com.sd.grpc.MedicationPlanResponse getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<MedicationPlanResponse>
      PARSER = new com.google.protobuf.AbstractParser<MedicationPlanResponse>() {
    @java.lang.Override
    public MedicationPlanResponse parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new MedicationPlanResponse(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<MedicationPlanResponse> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<MedicationPlanResponse> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public com.sd.grpc.MedicationPlanResponse getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

